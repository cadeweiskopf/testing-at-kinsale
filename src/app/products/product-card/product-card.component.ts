import { Component, Input, OnInit } from '@angular/core';
import { IProduct } from '../products.module';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css'],
})
export class ProductCardComponent implements OnInit {
  @Input() product: IProduct | undefined;

  ngOnInit(): void {
    if (!this.product) {
      throw Error('No product set for product-cart component');
    }
  }
}
