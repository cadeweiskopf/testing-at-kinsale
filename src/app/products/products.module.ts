import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductCardComponent } from './product-card/product-card.component';
import { RouterModule } from '@angular/router';

enum ProductCategories {
  'TShirt',
  'Shoes',
}
export interface IProduct {
  id: string;
  slug: string;
  name: string;
  category: ProductCategories;
  rate: number;
}
export const DUMMY_PRODUCTS = [
  {
    id: '1',
    slug: 'tshirt',
    name: 'T-Shirt',
    category: ProductCategories.TShirt,
    rate: 100,
  },
  {
    id: '2',
    slug: 'shoes',
    name: 'Shoes',
    category: ProductCategories.Shoes,
    rate: 100,
  },
];

@NgModule({
  declarations: [ProductCardComponent],
  imports: [CommonModule, RouterModule],
  exports: [ProductCardComponent],
})
export class ProductsModule {}
