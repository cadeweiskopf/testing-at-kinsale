import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './screens/home-screen/home.component';
import { ProductCardComponent } from './products/product-card/product-card.component';
import { ProductScreenComponent } from './screens/product-screen/product-screen.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'product/:productSlug', component: ProductScreenComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
