import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DropdownInputComponent } from './dropdown-input/dropdown-input.component';

@NgModule({
  declarations: [DropdownInputComponent],
  imports: [CommonModule],
})
export class FormControlsModule {}
