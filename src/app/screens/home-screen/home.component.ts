import { Component, OnInit } from '@angular/core';
import { DUMMY_PRODUCTS, IProduct } from 'src/app/products/products.module';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  products: IProduct[] = [];

  ngOnInit(): void {
    this.products = DUMMY_PRODUCTS;
  }
}
