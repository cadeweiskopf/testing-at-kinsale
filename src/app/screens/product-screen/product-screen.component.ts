import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DUMMY_PRODUCTS, IProduct } from 'src/app/products/products.module';

@Component({
  selector: 'app-product-screen',
  templateUrl: './product-screen.component.html',
  styleUrls: ['./product-screen.component.css'],
})
export class ProductScreenComponent implements OnInit {
  product: IProduct | undefined;

  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      console.log(params['productSlug']);
      if (!params['productSlug']) {
        throw Error('Missing productSlug param for activated route.');
      }

      this.product = DUMMY_PRODUCTS.find(
        (product) => product.slug === params['productSlug']
      );
      if (!this.product) {
        throw Error('No product able to be found by slug');
      }
    });
  }
}
