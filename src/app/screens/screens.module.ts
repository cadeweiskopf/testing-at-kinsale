import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home-screen/home.component';
import { ProductsModule } from '../products/products.module';
import { ProductScreenComponent } from './product-screen/product-screen.component';
import { ErrorScreenComponent } from './error-screen/error-screen.component';

@NgModule({
  declarations: [HomeComponent, ProductScreenComponent, ErrorScreenComponent],
  imports: [CommonModule, ProductsModule],
})
export class ScreensModule {}
